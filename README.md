# Gemini Price Deviation Alert

This project demo for monitoring and triggering deviation alert on trading pairs on Gemini platform. 

## Requirements
- Docker

# Usage

### Build the Docker Image

To build the Docker image, navigate to the directory (root of this repo)
``` sh 
docker build -t gemini-alert . 
```

# Run the Docker container

docker run -d --name gemini-alert gemini-alert

# Cusomize the run
'--threshold': Standard deviation threshold for alerts (defaults to 1)
'--pairs': Specify the trading pairs to monitor
'--dry-run': Use mock data for a dry run. 

Example: <br>
docker run -d --name gemini-alert gemini-alert python ./gemini_alert.py --threshold 1.5

Run in dry mode:
docker run -d --name gemini-alert gemini-alert python ./gemini_alert.py --dry-run



# Further improvements
- Introduce multi-threading (May not work well with scheduling) or asynchronous requests to fetch data. 
- Introduce caching to speedup
- Introduce some precalculations to not recalculate (memoization or some algorithm?)


# Challenges
1. Finding right API endpoints. 
    This helped a lot:
    https://algotrading101.com/learn/gemini-api-guide/
2. Schedule, argparse and logging api documentation took a bit. 
3. Mac M2 was giving a lot of struggle with build. 
4. Spend around 1.5-2 hours for coding, 2-3 hours for just researching APIs.