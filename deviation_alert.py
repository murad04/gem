import requests, json, statistics, logging, argparse, schedule, time, random
from datetime import datetime
from typing import List, Dict, Deque, Any, Optional
from collections import deque

API_URL = "https://api.gemini.com/v1"

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

prices: Dict[str, Deque[float]] = {}

def generate_mock_data(base_price: float, volatility: float, count: int) -> List[float]:
    return [round(random.gauss(base_price, base_price * volatility), 4) for _ in range(count)]

MOCK_DATA = {
    "btcusd": generate_mock_data(34000, 0.05, 24),
    "ethusd": generate_mock_data(2000, 0.05, 24),
    "btceth": generate_mock_data(17, 0.05, 24)
}

def fetch_trading_pairs() -> List[str]:
    response = requests.get(f"{API_URL}/symbols")
    if response.status_code != 200:
        logging.error(f"Error fetching trading pairs: {response.text}")
        return []
    return response.json()

def fetch_current_price(trading_pair: str, dry_run: bool) -> Optional[float]:
    if dry_run:
        return MOCK_DATA[trading_pair][-1]  # Current price
    response = requests.get(f"{API_URL}/pubticker/{trading_pair}")
    if response.status_code != 200:
        logging.error(f"Error fetching data for {trading_pair}: {response.text}")
        return None
    data = response.json()
    return float(data['last'])

def update_prices(prices: Dict[str, Deque[float]], trading_pair: str, current_price: float) -> None:
    if trading_pair not in prices:
        prices[trading_pair] = deque(maxlen=24)
    prices[trading_pair].append(current_price)

def generate_alert(trading_pair: str, prices: Deque[float], threshold: float) -> Dict[str, Any]:
    avg_price = statistics.mean(prices)
    std_deviation = statistics.stdev(prices)
    last_price = prices[-1]
    price_change_value = last_price - avg_price

    deviation_flag = std_deviation > threshold

    alert = {
        "timestamp": datetime.now().isoformat(),
        "log_level": "INFO" if deviation_flag else "DEBUG",
        "trading_pair": trading_pair,
        "deviation": deviation_flag,
        "data": {
            "last_price": last_price,
            "average_price": avg_price,
            "deviation": std_deviation,
            "price_change_value": price_change_value
        }
    }
    return alert

def process_pair(pair: str, threshold: float, prices: Dict[str, Deque[float]], dry_run: bool) -> None:
    current_price = fetch_current_price(pair, dry_run)
    if current_price is None:
        alert = {
            "timestamp": datetime.now().isoformat(),
            "log_level": "ERROR",
            "trading_pair": pair,
            "deviation": False,
            "data": {"error": f"Failed to fetch prices for {pair}"}
        }
        logging.error(json.dumps(alert, indent=4))
    else:
        update_prices(prices, pair, current_price)
        if len(prices[pair]) == 24:
            alert = generate_alert(pair, prices[pair], threshold)
            log_level = logging.INFO if alert['deviation'] else logging.DEBUG
            logging.log(log_level, json.dumps(alert, indent=4))

def main() -> None:
    parser = argparse.ArgumentParser(description="Gemini Price Deviation Alert")
    parser.add_argument('--threshold', type=float, default=1, help='Standard deviation threshold for alerts')
    parser.add_argument('--pairs', type=str, nargs='+', help='Trading pairs to monitor')
    parser.add_argument('--dry-run', action='store_true', help='Use mock data for dry run')

    args = parser.parse_args()
    threshold = args.threshold
    dry_run = args.dry_run
    if args.pairs:
        trading_pairs = args.pairs
    elif dry_run:
        trading_pairs = list(MOCK_DATA.keys())
    else:
        trading_pairs = fetch_trading_pairs()

    if dry_run:
        for pair in MOCK_DATA.keys():
            prices[pair] = deque(MOCK_DATA[pair], maxlen=24)
            print(f"Loaded mock data for {pair}: {prices[pair]}")

    for pair in trading_pairs:
        process_pair(pair, threshold, prices, dry_run)

def job() -> None:
    main()

if __name__ == "__main__":

    schedule.every(5).seconds.do(job)

    while True:
        schedule.run_pending()
        time.sleep(1)
